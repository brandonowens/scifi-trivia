$(document).ready(function(){

// Removes Start button when clicked
  $("#start").on("click",function() {
    $("#start").remove();
    game.loadQuestion();
  });

  // Run clicked(event) when clicking an .answer-button
  $("body").on("click",".answer-button",function(event) {
    game.clicked(event);
  });

  // Run reset() upon clicking reset button
  $("body").on("click","#reset",function() {
    game.reset();
  });

// Questions Array - each question is an object 
// including the answers(options), correct answer, and image
  var questions = [{
      question: "What is the number plate from the delorean in Back to the Future?",
      answers: ["Rewind", "Backtime", "Outatime", "DMC"],
      correctAnswer: "Outatime",
      image: "assets/images/backtofuture.jpg"
    },{
      question: "What is the name of the cat from Alien and Aliens?",
      answers: ["Misty","Jonesy","Tiger","Sam"],
      correctAnswer: "Jonesy",
      image: "assets/images/jonesy.jpg"
    },{
      question: "What is the name of the ice planet in Star War V The Empire Strikes Back?",
      answers: ["Hoth","Naboo","Dagobah","Tatooine"],
      correctAnswer: "Hoth",
      image: "assets/images/hoth.jpg"
    },{
      question: "What is the name of the Fifth Element's name in the film 'The Fifth Element'?",
      answers: ["Rachael","Ruby","Pris","Leeloo"],
      correctAnswer: "Leeloo",
      image: "assets/images/leeloo.jpg"
    },{
      question: "What is the material that is bonded to Wolverine's bones in the X-Men films?",
      answers: ["Iridium","Chromium","Adamantium","Titanium"],
      correctAnswer: "Adamantium",
      image: "assets/images/wolverine.jpg"
    },{
      question: "What is the halloween costume E.T wore in the film E.T?",
      answers: ["A Ghost","A Skeleton","A Clown","A Witch"],
      correctAnswer: "A Ghost",
      image: "assets/images/etghost.jpg"
    },{
      question: "What is the registration number of the starship enterprise?",
      answers: ["NCV-474439-G","NCC-1701","NCC-1305-E","NX-01-A"],
      correctAnswer: "NCC-1701",
      image: "assets/images/starship.jpg"
    }];

// Game Object - contains the elements that make up the game
  var game = {
    
// Properties that keep track of questions, answers, and counter
    questions: questions,
    currentQuestion: 0,
    counter: 30,
    correct: 0,
    incorrect: 0,
    unanswered: 0,

// Countdown Method
    countdown: function () {
      // Decrease the counter by 1 and update the #counter div
      game.counter--;
      $("#counter").html(game.counter);

      // Load timeUp() when counter reaches 0
      if (game.counter <= 0) {
        game.timeUp();
      };
    },

// Load Question Method
    loadQuestion: function() {
      // Load countdown() every 1 second
      timer = setInterval(game.countdown, 1000);

      // Post the current question to the page
      $("#subwrapper").html("<p>" + questions[game.currentQuestion].question + "</p>");

      // Post the counter
      $("#subwrapper").append("<p>Time Remaining: <span id='counter'>30</span> Seconds</p>");
      
      // Post the list of possible answers for the current question
      // Loop through the answers array for each question
      for (var i = 0; i < questions[game.currentQuestion].answers.length; i++) {
        
        // Append/Add a button with an id and data-name(holds the answer) based on i.
        $("#subwrapper").append('<button class="answer-button" id="button-'+i+'" data-name="' + questions[game.currentQuestion].answers[i]+'">' + questions[game.currentQuestion].answers[i] + '</button>');
      };
    },

// Next Question Method
    nextQuestion: function() {
      // Reset the counter and update the html
      game.counter = 30;
      $("#counter").html(game.counter);

      // Increase the question number by 1 and run loadQuestion();
      game.currentQuestion++;
      game.loadQuestion();
    },

// Time Up Method
    timeUp: function() {
      // Stop the timer
      clearInterval(timer);
      
      // Update the score
      game.unanswered++;

      // Display html showing the correct answer
      $("#subwrapper").html("<p>Out of Time!</p>");
      $("#subwrapper").append("<p>The Correct Answer was: " + questions[game.currentQuestion].correctAnswer + "</p>");
      $("#images").html("<img id='card-image' src='" + questions[game.currentQuestion].image + "'>");
      $("#rightTitle").html("<h5>" + questions[game.currentQuestion].correctAnswer + "</h5>");

      // Check for more questions and load the next if available
      if (game.currentQuestion == questions.length - 1) {
        setTimeout(game.results, 3000);
      } else {
        setTimeout(game.nextQuestion, 3000);
      };
    },

// Results Method
    results: function() {
      // Stop the timer
      clearInterval(timer);

      // Display the scores as html
      $("#subwrapper").html("<b>Game Complete!</b>");
      $("#subwrapper").append("<p>Correct: " + game.correct + "</p>");
      $("#subwrapper").append("<p>Incorrect: " + game.incorrect + "</p>");
      $("#subwrapper").append("<p>Unanswered: " + game.unanswered + "</p>");

      // Reset button
      $("#subwrapper").append("<button id='reset'>Reset</button>");
    },

// Clicked Method
    clicked: function(event) {
      // Stop the timer
      clearInterval(timer);

      // Compare the data-name of the clicked button(event.target) to the correct answer and run the corresponding method
      if ($(event.target).data("name") == questions[game.currentQuestion].correctAnswer) {
          game.answeredCorrectly();
      } else {
          game.answeredIncorrectly();
      };
    },

// Correct Answers Method
    answeredCorrectly: function() {
      // Stop the timer
      clearInterval(timer);
      
      // Update the score and display html
      game.correct++;
      $("#subwrapper").html("<p>Correct Answer!</p>");
      $("#images").html("<img id='card-image' src='" + questions[game.currentQuestion].image + "'>");
      $("#rightTitle").html("<h5>" + questions[game.currentQuestion].correctAnswer + "</h5>");
      
      // Check for more questions and load the next if available
      if (game.currentQuestion == questions.length - 1) {
        setTimeout(game.results, 3000);
      } else {
        setTimeout(game.nextQuestion, 3000);
      };

      $(".card-footer").empty();
    },

// Incorrect Answers Method
    answeredIncorrectly: function() {
      // Stop the timer
      clearInterval(timer);

      // Update the score and display html
      game.incorrect++;
      $("#subwrapper").html("<p>Incorrect Answer!</p>");
      $(".card-footer").html("<p>The Correct Answer was: " + questions[game.currentQuestion].correctAnswer + "</p>");
      $("#images").html("<img id='card-image' src='" + questions[game.currentQuestion].image + "'>");
      $("#rightTitle").html("<h5>" + questions[game.currentQuestion].correctAnswer + "</h5>");

      // Check for more questions and load the next if available
      if (game.currentQuestion == questions.length - 1) {
        setTimeout(game.results, 3000);
      } else {
        setTimeout(game.nextQuestion, 3000);
      };
    },

// Reset Method
    reset: function() {

      // Reset all scores and run loadQuestion();
      game.currentQuestion = 0;
      game.counter = 30;
      game.correct = 0;
      game.incorrect = 0;
      game.unanswered = 0;
      game.loadQuestion();

    }
  
  } // End of Game Object

});